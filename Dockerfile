FROM golang:1.18rc1 as BUILDER

RUN mkdir /app
ADD . /app
WORKDIR /app

RUN CGO_ENABLED=0 GOOS=linux go build -o app cmd/main.go

FROM alpine:3.15 AS prodaction
COPY --from=builder /app .
CMD ["./app"]