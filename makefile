lint:
	golangci-lint run

start:
	go build cmd/main.go
	./main