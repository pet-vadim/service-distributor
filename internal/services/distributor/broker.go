package distributor

type Broker interface {
	SendPingTest(t Test)
	Close()
}